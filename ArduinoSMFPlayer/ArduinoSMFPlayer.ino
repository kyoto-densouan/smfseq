// Mini SMF(Standard Midi File) Sequencer Sample Program
//
// SDカード内に格納したSMFファイル"playdat0.mid"～"playdat4.mid"をボタン押下で演奏します。
// 
// 以下のI/F/ライブラリを使用します
//  SDカードシールド
//  MIDI IFシールド、eVY1シールド
//  MSTimer2
//
// このサンプルでは上記のほか以下のピンを使用します。
//  デジタルpin2：演奏開始/停止（開始時に次のSMFファイルに切り替え）
//  デジタルpin3：ch番号を1(ch1～9)もしくは2(11～15)ずらして演奏、eVY1のch1がVocal固定のためGM音源として使用する場合にLOWにする。
//
// ArduinoUnoでRAM空き容量が350バイト以上空いていないとスタック破壊します。

#include "common.h"
#include "SmfSeq.h"
#include "IntervalCheck.h"
#include "IntervalCheckCounts.h"
#include "TimerOne.h"                //ライブラリマネージャでインストール

SMF_SEQ_TABLE        *pseqTbl;       //SMFシーケンサハンドル

//----------------------------------------------------------------------
//MIDIポートアクセス関数定義
// MidiFunc.c/hから呼び出されるMIDI I/Fアクセス関数の実体を記述する。
// 以下ではハードウェアシリアルを使用した場合の例を記述。
#include "MidiPort.h"
int   MidiPort_open()
{
  Serial.begin(D_MIDI_PORT_BPS);
  return(0);
}
void  MidiPort_close()
{
  Serial.end();
}
int   MidiPort_write( UCHAR data )
{
#ifdef DUMPMIDI
  Serial.print( "1:" );
  int n = (int)data;
  Serial.println( n, HEX );
#else
  Serial.write( data );
#endif
//  Serial.flush();
  return( 1 );
}
int   MidiPort_writeBuffer( UCHAR * pData, ULONG Len )
{
#ifdef DUMPMIDI
  int n;
  int i;
  Serial.print( Len );
  Serial.print( ":" );
  for( i=0; i<Len; i++ ){
    n =(int)pData[i];
    Serial.print( n, HEX );
  }
  Serial.println("");
#else
  Serial.write( pData, Len );
#endif
//  Serial.flush();
  return( Len );
}
//----------------------------------------------------------------------
//SMFファイルアクセス関数定義
// SmfSeq.c/hから呼び出されるSMFファイルへのアクセス関数の実体を記述する。
// 以下ではSDカードシールドライブラリに対し、ファイルポインタで直接読み出し位置を指定する方法での例を記述。
// ライブラリ自体の初期化はsetup関数に記述している。
#define D_SD_CHIP_SELECT_PIN   4
#include <SPI.h>
#include <SD.h>
#include "SmfFileAccess.h"

File  s_FileHd;
bool SmfFileAccessOpen( UCHAR * Filename )
{
  bool  result = false;

  if( Filename!=NULL ){
    DPRINT(F("filename:"));
    DPRINTLN((const char *)Filename);
    s_FileHd = SD.open( (const char *)Filename );

    result = s_FileHd.available();
  }
  return( result );
}
void SmfFileAccessClose()
{
  s_FileHd.close();
}
bool SmfFileAccessRead( UCHAR * Buf, unsigned long Ptr )
{
  bool  result = true;
  if( Buf!=NULL ){
    if( s_FileHd.position() != Ptr ){
      s_FileHd.seek(Ptr);
    }
    int data = s_FileHd.read();
    if( data>=0 ){
      *Buf = (UCHAR)data;
    }else{
      result = false;
    }
  }
  return( result );
}
bool SmfFileAccessReadNext( UCHAR * Buf )
{
  bool  result = true;
  if( Buf!=NULL ){
    int data = s_FileHd.read();
    if( data>=0 ){
      *Buf = (UCHAR)data;
    }else{
      result = false;
    }
  }
  return( result );
}
int SmfFileAccessReadBuf( UCHAR * Buf, unsigned long Ptr, int Lng )
{
  int result = 0;
  if( Buf!=NULL ){
    if( s_FileHd.position() != Ptr ){
      s_FileHd.seek(Ptr);
    }

    int i;
    int data;
    for( i=0; i<Lng; i++ ){
      data = s_FileHd.read();
      if( data>=0 ){
        Buf[i] = (UCHAR)data;
        result++;
      }else{
        break;
      }
    }
  }
  return( result );
}
unsigned int SmfFileAccessSize()
{
  unsigned int result = 0;
  result = s_FileHd.size();
  
  return( result );
}
//----------------------------------------------------------------------
#define D_PLAY_BUTTON_PIN   2  //開始/停止ボタン
#define D_CH_OFFSET_PIN     3  //チャンネル番号オフセット（eVY1のGM音源としての演奏）
#define D_FF_BUTTON_PIN     5  //送りボタン
#define D_STATUS_LED        10 //状態表示LED

int playdataCnt = 0;        //選曲番号
#define D_PLAY_DATA_NUM     10  //SDカード格納ＳＭＦファイル数

IntervalCheck sButtonCheckInterval( 100, true );
//IntervalCheck sTickProcInterval( ZTICK, true );             //1ms単位でのΔt監視
IntervalCheckCounts sTickProcInterval( ZTICK * 10, true );  //100us単位でのΔt監視
IntervalCheck sStatusLedCheckInterval( 100, true );
unsigned int  sLedPattern = 0x0f0f;

//SMFファイル名生成
// playdat0.mid～playdat9.midの文字列を順次返す。
char * makeFilename() {
  int cnt = 0;
  char * filename = (char*)"playdat0.mid";
  for( cnt=0; cnt < D_PLAY_DATA_NUM; cnt++ ){
    filename[7] = 0x30 + playdataCnt;
    playdataCnt++;
    if( playdataCnt>=D_PLAY_DATA_NUM ){
      playdataCnt = 0;
    }
    if( SD.exists(filename) == true ){
      break;
    }
  }
  return( filename );
}

//TimerOneによる100us単位でのΔt監視
void intFunc() {
  // Δt生成カウンタ更新
  sTickProcInterval.updateCount();
}

void setup() {
  Serial.begin( D_MIDI_PORT_BPS );

  pinMode( D_PLAY_BUTTON_PIN, INPUT_PULLUP );
  pinMode( D_FF_BUTTON_PIN, INPUT_PULLUP );
  pinMode( D_CH_OFFSET_PIN, INPUT_PULLUP );
  pinMode( D_STATUS_LED, OUTPUT );
  
  DPRINTLN(F("Initializing SD card..."));

  if (!SD.begin(D_SD_CHIP_SELECT_PIN)) {
    DPRINTLN(F("Card failed, or not present"));
    // don't do anything more:
    return;
  }
  DPRINTLN(F("card initialized."));
  // すぐにファイルアクセスするとフォーマット破壊することがあったため待ち
  delay(2000);

  digitalWrite( D_STATUS_LED, HIGH );
  int      Ret;
  pseqTbl = SmfSeqInit( ZTICK );
  if( pseqTbl==NULL ){
    DPRINTLN(F("SmfSeqInit failed."));
    return;    
  }

  int chNoOffset = 0;
  if( digitalRead( D_CH_OFFSET_PIN )==LOW ){
    //CH番号を1ずらす（eVY1のCH0が音声合成専用のため、GM音源として使用する場合CH0を使用しない）
    chNoOffset = 1;
  }

  //SMFファイル読込
  SmfSeqFileLoadWithChNoOffset( pseqTbl, (char*)makeFilename(), chNoOffset );
  //GMリセット送信
  Ret = SmfSeqGMReset();
  //発音中全キーノートオフ
  Ret = SmfSeqAllNoteOff( pseqTbl );
  //トラックテーブルリセット
  SmfSeqPlayResetTrkTbl( pseqTbl );
  //演奏開始
  SmfSeqStart( pseqTbl );
  digitalWrite( D_STATUS_LED, LOW );

  sButtonCheckInterval.reset();
  sTickProcInterval.reset();

  //TimerOneによる100us単位でのΔt監視
  Timer1.initialize(100);
  Timer1.attachInterrupt(intFunc);
}

int prePlayButtonStatus = HIGH;
int preFfButtonStatus = HIGH;
void loop() {
  int      Ret;

  // 定期起動処理
  if( sTickProcInterval.check() == true ){
    if( SmfSeqGetStatus( pseqTbl ) != SMF_STAT_STOP ){
      //状態が演奏停止中以外の場合
      //定期処理を実行
      Ret = SmfSeqTickProc( pseqTbl );
      //処理が間に合わない場合のリカバリ
      while( sTickProcInterval.check() == true ){
        //定期処理を実行
        Ret = SmfSeqTickProc( pseqTbl );
      }
      if( SmfSeqGetStatus( pseqTbl )  == SMF_STAT_STOP ){
        //状態が演奏停止中になった場合
        //発音中全キーノートオフ
        Ret = SmfSeqAllNoteOff( pseqTbl );
        //トラックテーブルリセット
        SmfSeqPlayResetTrkTbl( pseqTbl );
        // ファイルクローズ
        SmfSeqEnd( pseqTbl );
        DPRINTLN(F("SEQ end."));

        digitalWrite( D_STATUS_LED, HIGH );
        int chNoOffset = 0;
        if( digitalRead( D_CH_OFFSET_PIN )==LOW ){
          //CH番号を1ずらす（eVY1のCH0が音声合成専用のため、GM音源として使用する場合CH0を使用しない）
          chNoOffset = 1;
        }
          
        pseqTbl = SmfSeqInit( ZTICK );
        //SMFファイル読込
        SmfSeqFileLoadWithChNoOffset( pseqTbl, (char*)makeFilename(), chNoOffset );
        //GMリセット送信
        Ret = SmfSeqGMReset();
        //発音中全キーノートオフ
        Ret = SmfSeqAllNoteOff( pseqTbl );
        //トラックテーブルリセット
        SmfSeqPlayResetTrkTbl( pseqTbl );
        //演奏開始
        SmfSeqStart( pseqTbl );
        digitalWrite( D_STATUS_LED, LOW );
        sTickProcInterval.reset();
      }
    }
  }

  // ボタン操作処理
  if( sButtonCheckInterval.check() == true ){
    //スイッチ状態取得
    int buttonPlayStatus = digitalRead( D_PLAY_BUTTON_PIN );
    if( prePlayButtonStatus != buttonPlayStatus ){
      //スイッチ状態が変化していた場合
      if( buttonPlayStatus == LOW  ){
        //スイッチ状態がONの場合
        if( SmfSeqGetStatus( pseqTbl ) == SMF_STAT_STOP ){
          //演奏開始
          SmfSeqStart( pseqTbl );
          sTickProcInterval.reset();
        }else{
          //演奏中なら演奏停止
          SmfSeqStop( pseqTbl );
          //発音中全キーノートオフ
          Ret = SmfSeqAllNoteOff( pseqTbl );
        }
      }
    }
    //スイッチ状態保持
    prePlayButtonStatus = buttonPlayStatus;
  
    int buttonFfStatus = digitalRead( D_FF_BUTTON_PIN );
    if( preFfButtonStatus != buttonFfStatus ){
      //スイッチ状態が変化していた場合
      if( preFfButtonStatus == LOW  ){    
        //スイッチ状態がONの場合
        bool  playing = false;
        if( SmfSeqGetStatus( pseqTbl ) != SMF_STAT_STOP ){
          //演奏中なら演奏停止
          SmfSeqStop( pseqTbl );
          //発音中全キーノートオフ
          Ret = SmfSeqAllNoteOff( pseqTbl );
          //トラックテーブルリセット
          SmfSeqPlayResetTrkTbl( pseqTbl );
          // ファイルクローズ
          SmfSeqEnd( pseqTbl );
          playing = true;
        }else{
          playing = false;
        }
        
        digitalWrite( D_STATUS_LED, HIGH );
        int chNoOffset = 0;
        if( digitalRead( D_CH_OFFSET_PIN )==LOW ){
          //CH番号を1ずらす（eVY1のCH0が音声合成専用のため、GM音源として使用する場合CH0を使用しない）
          chNoOffset = 1;
        }
          
        pseqTbl = SmfSeqInit( ZTICK );
        //SMFファイル読込
        SmfSeqFileLoadWithChNoOffset( pseqTbl, (char*)makeFilename(), chNoOffset );
        //GMリセット送信
        Ret = SmfSeqGMReset();
        //発音中全キーノートオフ
        Ret = SmfSeqAllNoteOff( pseqTbl );
        //トラックテーブルリセット
        SmfSeqPlayResetTrkTbl( pseqTbl );
        if( playing == true ){
          //演奏開始
          SmfSeqStart( pseqTbl );
          sTickProcInterval.reset();
        }
        digitalWrite( D_STATUS_LED, LOW );
      }
    }
    //スイッチ状態保持
    preFfButtonStatus = buttonFfStatus;
  }

  // 状態表示更新
  if( SmfSeqGetStatus( pseqTbl ) != SMF_STAT_STOP ){
    if( sStatusLedCheckInterval.check() == true ){
      unsigned int led = sLedPattern & 0x0001;
      if( led > 0 ){
        digitalWrite( D_STATUS_LED, HIGH );
      }else{
        digitalWrite( D_STATUS_LED, LOW );
      }
      sLedPattern = (sLedPattern >> 1) | (led << 15);
    }
  }else{
    digitalWrite( D_STATUS_LED, LOW );
  }
}


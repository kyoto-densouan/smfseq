# Mini SMF(Standard Midi File) Sequencer Sample Program for Arduino #

## 概要 ##

 ArduinoでSDカード内のSMFファイルの演奏を行います。
 SDカード内に格納したSMFファイル"playdat0.mid"～"playdat9.mid"をボタン押下で演奏します。
 
## 使用シールド・ライブラリ ##

 以下のI/F/ライブラリを使用します（ライブラリマネージャを使用してインストールしてください）
 
   * TimerOne

 必要に応じて以下のシールドを使用します
 
   * SDカードシールド
   * MIDI IFシールド、eVY1シールド

## そのほかの結線 ##

 このサンプルでは上記のほか以下のピンを使用します。

   * デジタルpin2：演奏開始/停止
   * デジタルpin3：ch番号を1(ch1～9)もしくは2(11～15)ずらして演奏、eVY1のch1がVocal固定のためGM音源として使用する場合にLOWにする。
   * デジタルpin5：次のSMFファイルに変更
   * デジタルpin10：LED（点灯：SMFファイル読み込み中/点滅：演奏中/消灯：停止）

##  サンプル動画 ##

   * Mini SMF Sequencerでてってってー(https://www.youtube.com/watch?v=ncJrpP76aqc)
   * Mini SMF sequencerでeVY1を鳴らしてみた(https://youtu.be/MLQSQ4wQZ_A)
   * Mini SMF sequencer. Play midi file on ArduinoUno. (https://youtu.be/MkvfWXkfQNU)

## 制限 ##

* ArduinoUnoでRAM空き容量が350バイト以上空いていないとスタック破壊するため正常に動作しません。
* トラック数が多いSMFファイルの場合、処理が間に合わずにテンポがずれます。(転送速度の速いSDカードを使用することで軽減します）。